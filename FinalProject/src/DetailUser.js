import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button, ScrollView } from 'react-native';
import MCI from "@expo/vector-icons/MaterialCommunityIcons";
import MC from "@expo/vector-icons/MaterialIcons";
import Axios from 'axios';
import Navbar from './components/navbar';

// import data from './myapi-abcbb.json';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoading: true,
            isError: false,
            nama: '', work: '', address: '', bd: '', school: '', fb: '', ig: '', github: '', gitlab: '', web: '', image_prof: 'https://i1.wp.com/static.teamtreehouse.com/assets/content/default_avatar-ea7cf6abde4eec089a4e03cc925d0e893e428b2b6971b12405a9b118c837eaa2.png?ssl=1',
            link_fb: '', link_tw: '', link_ig: '', link_gl: '', link_gh: '', link_web: '', bd: '', phone: '', email: ''
        }
    }

    componentDidMount() {
        this.getGithubUser()
    }

    getGithubUser = async () => {
        try {
            const response = await Axios.get(`https://myapi-abcbb.firebaseio.com/profile.json`)
            // this.setState({ isError: false, isLoading: false, data: response.data })
            // alert(this.state.searchText)
            this.setState({ nama: response.data[0].name })
            this.setState({ work: response.data[0].work_in })
            this.setState({ address: response.data[0].addrees })
            this.setState({ bd: response.data[0].date_borthday })
            this.setState({ school: response.data[0].last_school })
            this.setState({ fb: response.data[0].facebook })
            this.setState({ twitter: response.data[0].twitter })
            this.setState({ ig: response.data[0].instagram })
            this.setState({ github: response.data[0].github })
            this.setState({ gitlab: response.data[0].gitlab })
            this.setState({ web: response.data[0].web })
            this.setState({ link_fb: response.data[0].link_facebook })
            this.setState({ link_tw: response.data[0].link_twitter })
            this.setState({ link_ig: response.data[0].link_instagram })
            this.setState({ image_prof: response.data[0].image_profile })
            this.setState({ link_gl: response.data[0].link_gitlab })
            this.setState({ link_gh: response.data[0].link_github })
            this.setState({ link_web: response.data[0].link_web })
            this.setState({ bd: response.data[0].date_borthday })
            this.setState({ phone: response.data[0].phone })
            this.setState({ email: response.data[0].email })
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {/* <Navbar onPress={() => this.props.navigation.toggleDrawer()} /> */}
                <ScrollView>
                    <View style={styles.containerImage}>
                        <View style={{ width: 90, height: 90, marginTop: 20, borderRadius: 50, backgroundColor: 'white', elevation: 10, justifyContent: 'center', alignItems: 'center', marginLeft: 20 }}>
                            <Image source={{ uri: this.state.image_prof }} style={{ width: 93, height: 93, borderRadius: 50 }} />
                        </View>
                        <Text style={{ fontSize: 20, fontWeight: 'normal', marginLeft: 20 }}>{this.state.nama}</Text>
                    </View>
                    <View style={styles.containerBio}>
                        <View style={styles.lineStyle} />
                        <View style={styles.boxBio}>
                            <MC name="account-circle" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Full name</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.nama}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="home" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Address</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.address}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="date-range" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Birthday</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.bd}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="phone" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Phone</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.phone}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="email" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Email</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.email}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.containerBio}>
                        <View style={styles.lineStyle} />
                        <View style={styles.boxBio}>
                            <MC name="work" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Working in</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.work}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="school" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Last school</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.school}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="home" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Address</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.address}</Text>
                            </View>
                        </View>
                    </View>

                </ScrollView>
                {/* <StatusBar /> */}
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },

    containerImage: {
        alignItems: 'center',
        flexDirection: "row"
    },
    lineStyle: {
        borderWidth: 1,
        width: 330,
        borderColor: '#980080',
        elevation: 2,
        marginTop: 10
    },

    containerBio: {
        padding: 10
    },
    boxBio: {
        flexDirection: 'row',
        marginTop: 10
    }

})
