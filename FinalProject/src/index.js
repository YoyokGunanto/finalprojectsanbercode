import { StyleSheet, Text, View } from 'react-native';
import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import RegistrasiScreen from './RegistrasiScreen';
import HomeScreen from './HomeScreen';
import AboutScreen from './AboutScreen';
import DetailFilm from './DetailFilm';
import DetailUser from './DetailUser';

// import { SignIn, CreateAccount, Home, Search, Details, Search2, Profile } from './Screen';

const HomeStack = createStackNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const HomeStackScreen = () => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
            <HomeStack.Screen name="Details" component={DetailFilm} options={{ title: 'Detail Film' }}
                options={({ route }) => ({
                    title: route.params.name
                })}
            />
            <HomeStack.Screen name="DetailsUser" component={DetailUser} options={{ title: 'Detail User' }}
                options={({ route }) => ({
                    title: route.params.name
                })}
            />
        </HomeStack.Navigator>
    );
}

const DrawerHomeScreen = () => {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="About Me" component={AboutScreen} />
            <Drawer.Screen name="Home" component={HomeStackScreen} />
            <Drawer.Screen name="Logout" component={LoginScreen} />
            {/* <Drawer.Screen name="Details" component={Details} options={({ route }) => ({
        title: route.params.name
      })} /> */}
        </Drawer.Navigator>
    );
}

export default class App extends React.Component {
    render() {
        return (

            <NavigationContainer>
                <Stack.Navigator initialRouteName="Login" >
                    <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
                    <Stack.Screen name='Registrasi' component={RegistrasiScreen} options={{ headerShown: false }} />
                    <Stack.Screen name='Home' component={DrawerHomeScreen} options={{ headerShown: false }} />
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}
