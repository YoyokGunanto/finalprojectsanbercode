import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, ScrollView, Button } from 'react-native';
import MCI from "@expo/vector-icons/MaterialCommunityIcons";
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
    name: state.name,
})

export class LoginScreen extends React.Component {
    // const globalState = useSelector(state => state); //{globalState.name}
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            isError: false,
        }
    }


    loginHandler() {

        // if (this.state.password == '12345678' && this.state.userName != '') {
        //   this.setState({
        //     isError: false
        //   });
        //   this.props.navigation.navigate('Home', {
        //     userName: this.state.userName
        //   });
        // } else {
        //   this.setState({
        //     isError: true
        //   });
        // }
        if (this.state.password == '' && this.state.userName == '') {
            alert('Masukkan username dan password');
        } else if (this.state.userName == '') {
            alert('Masukkan username');
        } else if (this.state.password == '') {
            alert('Masukkan password');
        } else {
            this.props.navigation.navigate('Home', {
                screen: 'Home',
                params: {
                    userName: this.state.userName
                }
                // userName: this.state.userName
            });
        }

    }


    render() {

        const { name } = this.props

        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <MCI name="account-circle" color='#980080' size={40} />
                    <Text style={{ color: '#980080', fontWeight: 'bold', fontSize: 25 }}>{this.props.name}</Text>
                </View>
                <View>
                    <View style={styles.inputContainer}>
                        <TextInput style={styles.textInput}
                            placeholder='Email'
                            onChangeText={userName => this.setState({ userName })}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.textInput}
                            placeholder='Password'
                            onChangeText={password => this.setState({ password })}
                            secureTextEntry={true}
                            autoCompleteType="password"
                        />
                        <TouchableOpacity>
                            <Text style={{ color: '#980080', fontSize: 15, marginTop: 5, textAlign: 'right' }}>Forgot password?</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.buttonLogin} onPress={() => this.loginHandler()}>
                        <MCI color="white" name="login-variant" size={20} />
                        <Text style={styles.buttonText}> Sign In</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Registrasi')}>
                        <Text style={{ color: '#980080', fontSize: 15, marginTop: 10 }}>Don't have account? Sign Up</Text>
                    </TouchableOpacity>
                </View>
                {/* <StatusBar /> */}
            </View>
            // <View style={styles.container}>
            //   <View>
            //     <Text style={styles.titleText}>Soal Quiz 3</Text>
            //     <Text style={styles.subTitleText}>Sanbercode</Text>
            //   </View>

            //   <View style={styles.formContainer}>
            //     <View style={styles.inputContainer}>
            //       <MaterialCommunityIcons name='account-circle' color='blue' size={40} />
            //       <View>
            //         <Text style={styles.labelText}>Username/Email</Text>
            //         <TextInput
            //           style={styles.textInput}
            //           placeholder='Masukkan Nama User/Email'
            //           onChangeText={userName => this.setState({ userName })}
            //         />
            //       </View>
            //     </View>

            //     <View style={styles.inputContainer}>
            //       <MaterialCommunityIcons name='lock' color='blue' size={40} />
            //       <View>
            //         <Text style={styles.labelText}>Password</Text>
            //         <TextInput
            //           style={styles.textInput}
            //           placeholder='Masukkan Password'
            //           onChangeText={password => this.setState({ password })}
            //           secureTextEntry={true}
            //         />
            //       </View>
            //     </View>
            //     <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Username atau Password Salah</Text>
            //     <Button title='Login' onPress={() => this.loginHandler()} />
            //   </View>
            // </View>
        )
    }
};

export default connect(mapStateToProps)(LoginScreen)

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F169CB',
        paddingBottom: 200,
    },
    logoContainer: {
        marginTop: 150,
        alignItems: 'center',
    },
    inputContainer: {
        marginTop: 20,
    },
    textInput: {
        width: 280,
        height: 38,
        borderColor: '#980080',
        borderWidth: 1,
        elevation: 6,
        borderRadius: 30,
        padding: 10,
        backgroundColor: 'white',
    },
    buttonContainer: {
        alignItems: 'center',
        paddingTop: 25,
    },
    buttonLogin: {
        height: 40,
        width: 150,
        marginTop: 10,
        backgroundColor: '#980080',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        elevation: 5,
    },
    buttonText: {
        color: 'white',
        fontSize: 18,
    }
    // container: {
    //   flex: 2,
    //   justifyContent: 'center',
    //   alignItems: 'center',
    // },
    // titleText: {
    //   fontSize: 48,
    //   fontWeight: 'bold',
    //   color: 'blue',
    //   textAlign: 'center',
    // },
    // subTitleText: {
    //   fontSize: 24,
    //   fontWeight: 'bold',
    //   color: 'blue',
    //   alignSelf: 'flex-end',
    //   marginBottom: 16
    // },
    // formContainer: {
    //   justifyContent: 'center'
    // },
    // inputContainer: {
    //   flexDirection: 'row',
    //   alignItems: 'center',
    //   alignSelf: 'center',
    //   marginBottom: 16
    // },
    // labelText: {
    //   fontWeight: 'bold'
    // },
    // textInput: {
    //   width: 300,
    //   backgroundColor: 'white'
    // },
    // errorText: {
    //   color: 'red',
    //   textAlign: 'center',
    //   marginBottom: 16,
    // },
    // hiddenErrorText: {
    //   color: 'transparent',
    //   textAlign: 'center',
    //   marginBottom: 16,
    // }
});
