import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import MCI from "@expo/vector-icons/MaterialCommunityIcons";
import MC from "@expo/vector-icons/MaterialIcons";
import Axios from 'axios';
import DataList from './ListItem';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            searchText: '',
            data: {},
            isLoading: true,
            isError: false
        }
    }

    componentDidMount() {
        this.getGithubUser()
    }

    getGithubUser = async () => {
        try {
            const response = await Axios.get(`http://www.omdbapi.com/?apikey=2cd2f26e&s=${this.state.searchText}`)
            this.setState({ isError: false, isLoading: false, data: response.data })
            // alert(tresponse.data.Search[0].Type
            // console.log(response.data.Search);
            // alert(this.state.searchText);
            this.setState({ data: response.data });
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    render() {
        // console.log(this.state.data.Year);
        // console.log(this.state.data.imdbID);
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} >
                        <MCI color="#980080" name="menu" size={30} />
                    </TouchableOpacity>
                    <TextInput style={styles.inputSearch}
                        placeholder='Search'
                        onChangeText={searchText => this.setState({ searchText })}
                    />
                    <TouchableOpacity style={styles.buttonSearch} onPress={() => this.getGithubUser()}>
                        <MC color="#929292" name="search" size={30} />
                    </TouchableOpacity>
                </View>
                <View style={styles.containerItems}>
                    <FlatList
                        data={this.state.data.Search}
                        renderItem={(item) => <DataList
                            listFilm={item.item}
                            // press={() => this.props.navigation.push('Details', {
                            //     name: 'Detail Film',
                            //     id: item.imdbID,
                            // })} />
                            press={
                                () => {
                                    // console.log(item.item.imdbID);
                                    this.props.navigation.navigate('Details', {
                                        //screen: 'Details',
                                        //params: {
                                        id_film: item.item.imdbID
                                        //}
                                    })
                                }
                            } />
                        }
                        keyExtractor={(item) => item.imdbID.toString()}
                    // ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
                    />
                </View>
                {/* <StatusBar /> */}
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    navBar: {
        height: 60,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputSearch: {
        width: 250,
        height: 35,
        // borderColor: '#980080',
        // borderBottomWidth: 1,
        // elevation: 5,
        // borderRadius: 30,
        padding: 10,
        backgroundColor: 'white',
        // borderBottomColor: '#929292',
    },

    containerItems: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemContainer: {
        width: DEVICE.width * 0.44,
        padding: 6,
        margin: 5,
        paddingBottom: 20,
        borderRadius: 5,
        backgroundColor: 'white',
        elevation: 3,
        justifyContent: 'space-around',
    },
    itemImage: {
        width: 100,
        height: 100,
        alignSelf: 'center',
    },
    itemName: {
        fontSize: 14,
        marginTop: 8,
        color: '#757575',
        fontWeight: 'bold',
    },
    itemPrice: {
        fontSize: 13,
        color: '#00b300',
        textAlign: 'center',
    },
    itemStock: {
        fontSize: 14,
        color: 'grey',
        textAlign: 'center',
    },
    itemButton: {
        width: 80,
        height: 35,
        marginTop: 10,
        backgroundColor: '#4c8bff',
        alignSelf: 'center',
    },
    buttonText: {
        color: 'white',
        marginTop: 5,
        alignSelf: 'center',
    },

    viewList: {
        height: 100,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DDD',
        alignItems: 'center'
    },
    Image: {
        width: 88,
        height: 80,
        borderRadius: 40
    },
    textItemLogin: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        marginLeft: 20,
        fontSize: 16
    },
    textItemUrl: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 12,
        marginTop: 10,
        color: 'blue'
    }

})
