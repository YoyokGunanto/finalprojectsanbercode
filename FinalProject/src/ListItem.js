import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';


export default class ListItem extends React.Component {

    render() {
        // console.log(this.props.listFilm.imdbID);
        return (
            <TouchableOpacity onPress={this.props.press} id_film={this.props.listFilm.imdbID}>
                <View style={styles.container}>
                    <Image source={{ uri: this.props.listFilm.Poster }} style={styles.imageItem} />
                    <Text style={styles.textItem}>{this.props.listFilm.Title}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export const Details = ({ route }) => (
    <ScreenContainer>
        <Text>Details Screen</Text>
        {route.params.name && <Text>{route.params.name}</Text>}
    </ScreenContainer>
);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 13,
        width: 330,
        padding: 10,
        elevation: 5,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageItem: {
        width: 300,
        height: 200,
        borderRadius: 2
    },
    textItem: {
        fontSize: 18,
        marginTop: 5,
        alignSelf: 'flex-start'
    }
});
