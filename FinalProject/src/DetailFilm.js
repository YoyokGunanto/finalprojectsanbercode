import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button, ScrollView } from 'react-native';
import MCI from "@expo/vector-icons/MaterialCommunityIcons";
import MC from "@expo/vector-icons/MaterialIcons";
import Axios from 'axios';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id_film: this.props.route.params.id_film,//tt1201607
            data: {},
            isLoading: true,
            isError: false,
        }
    }

    componentDidMount() {
        this.getGithubUser()
    }

    getGithubUser = async () => {
        try {
            const response = await Axios.get(`http://www.omdbapi.com/?apikey=2cd2f26e&i=${this.state.id_film}`)
            // this.setState({ isError: false, isLoading: false, data: response.data })
            this.setState({ data: response.data })
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    render() {
        // console.log(this.props.route.params.id_film)
        // console.log(this.state.data.Poster)
        return (
            <View style={styles.container}>
                <Image source={{ uri: this.state.data.Poster }} style={{ width: 330, height: 200, marginTop: 10, borderRadius: 5 }} />
                <ScrollView style={{ backgroundColor: 'white', borderRadius: 5, marginTop: 10, padding: 10, elevation: 10 }}>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Judul</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Title}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Genre</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Genre}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Type</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Type}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Duration</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Runtime}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Language</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Language}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Released</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Released}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Writer</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Writer}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Director</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Director}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Production</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Production}</Text>
                    </View>
                    <View style={styles.contentDetail}>
                        <Text style={styles.title}>Plot</Text>
                        <Text> : </Text>
                        <Text style={styles.detail}> {this.state.data.Plot}</Text>
                    </View>
                    <View>
                        <Text></Text>
                        <Text></Text>
                    </View>
                </ScrollView>
                {/* <StatusBar /> */}
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
    },
    contentDetail: {
        flexDirection: 'row',
        marginTop: 10
    },
    title: {
        fontSize: 15,
        marginLeft: 15,
        // backgroundColor: 'red',
        width: 70,
        color: 'gray'
    },
    detail: {
        fontSize: 15,
        marginLeft: 5,
        width: 220,
        color: 'gray'
        // backgroundColor: 'red',
    }

})
