import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import MCI from "@expo/vector-icons/MaterialCommunityIcons";
import MC from "@expo/vector-icons/MaterialIcons";

export default class Navbar extends React.Component {

    render() {
        return (
            <View style={styles.navBar}>
                <TouchableOpacity onPress={this.props.onPress} >
                    <MCI color="#980080" name="menu" size={30} />
                </TouchableOpacity>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    navBar: {
        height: 60,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputSearch: {
        width: 250,
        height: 35,
        // borderColor: '#980080',
        // borderBottomWidth: 1,
        // elevation: 5,
        // borderRadius: 30,
        padding: 10,
        backgroundColor: 'white',
        // borderBottomColor: '#929292',
    },

    containerItems: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

})
