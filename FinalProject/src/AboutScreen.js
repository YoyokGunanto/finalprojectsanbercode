import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button, ScrollView } from 'react-native';
import MCI from "@expo/vector-icons/MaterialCommunityIcons";
import MC from "@expo/vector-icons/MaterialIcons";
import Axios from 'axios';
import Navbar from './components/navbar';

// import data from './myapi-abcbb.json';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoading: true,
            isError: false,
            nama: '', work: '', address: '', bd: '', school: '', fb: '', ig: '', github: '', gitlab: '', web: '', image_prof: 'https://i1.wp.com/static.teamtreehouse.com/assets/content/default_avatar-ea7cf6abde4eec089a4e03cc925d0e893e428b2b6971b12405a9b118c837eaa2.png?ssl=1',
            link_fb: '', link_tw: '', link_ig: '', link_gl: '', link_gh: '', link_web: ''
        }
    }

    componentDidMount() {
        this.getGithubUser()
    }

    getGithubUser = async () => {
        try {
            const response = await Axios.get(`https://myapi-abcbb.firebaseio.com/profile.json`)
            // this.setState({ isError: false, isLoading: false, data: response.data })
            // alert(this.state.searchText)
            this.setState({ nama: response.data[0].name })
            this.setState({ work: response.data[0].work_in })
            this.setState({ address: response.data[0].addrees })
            this.setState({ bd: response.data[0].date_borthday })
            this.setState({ school: response.data[0].last_school })
            this.setState({ fb: response.data[0].facebook })
            this.setState({ twitter: response.data[0].twitter })
            this.setState({ ig: response.data[0].instagram })
            this.setState({ github: response.data[0].github })
            this.setState({ gitlab: response.data[0].gitlab })
            this.setState({ web: response.data[0].web })
            this.setState({ link_fb: response.data[0].link_facebook })
            this.setState({ link_tw: response.data[0].link_twitter })
            this.setState({ link_ig: response.data[0].link_instagram })
            this.setState({ image_prof: response.data[0].image_profile })
            this.setState({ link_gl: response.data[0].link_gitlab })
            this.setState({ link_gh: response.data[0].link_github })
            this.setState({ link_web: response.data[0].link_web })
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Navbar onPress={() => this.props.navigation.toggleDrawer()} />
                <ScrollView>

                    <View style={styles.containerImage}>
                        <Image source={require('./assets/background.jpg')} style={{ width: 350, height: 160, marginTop: 8, borderRadius: 8 }} />
                        <View style={{ width: 100, height: 100, marginTop: -50, borderRadius: 50, backgroundColor: 'white', elevation: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={{ uri: this.state.image_prof }} style={{ width: 93, height: 93, borderRadius: 50 }} />
                        </View>
                        <Text style={{ fontSize: 20, fontWeight: 'normal', marginTop: 10 }}>{this.state.nama}</Text>
                        <View style={styles.lineStyle} />
                    </View>
                    <View style={styles.containerBio}>
                        <View style={styles.boxBio}>
                            <MC name="work" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Working in</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.work}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="school" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Last school</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.school}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MC name="home" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>Address</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.address}</Text>
                            </View>
                        </View>
                        <View style={{ alignItems: 'center', }}>
                            <TouchableOpacity style={{ marginTop: 10, backgroundColor: '#980080', width: 100, height: 30, borderRadius: 5, alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }} onPress={
                                () => this.props.navigation.navigate('DetailsUser', {
                                    name: 'Detail Biodata'
                                }
                                )
                            }>
                                <Text style={{ color: 'white' }}>Show More </Text>
                                <MCI name="arrow-right-bold-circle" style={{ fontSize: 18, color: 'white' }} />
                            </TouchableOpacity>
                            <View style={styles.lineStyle} />
                        </View>
                    </View>
                    <View style={styles.containerBio}>
                        <Text style={{ marginLeft: 10, fontSize: 18, marginBottom: 10 }}>Social Media</Text>
                        <View style={styles.boxBio}>
                            <MCI name="facebook-box" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.fb}</Text>
                                <Text style={{ fontSize: 15, marginLeft: 9, color: '#929292' }}>{this.state.link_fb}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MCI name="twitter-circle" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.twitter}</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>{this.state.link_tw}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MCI name="instagram" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.ig}</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292', marginBottom: 10 }}>{this.state.link_ig}</Text>
                            </View>
                        </View>
                        <View style={{ alignItems: 'center', }}>
                            <View style={styles.lineStyle} />
                        </View>
                    </View>
                    <View style={styles.containerBio}>
                        <Text style={{ marginLeft: 10, fontSize: 18, marginBottom: 10 }}>Portofolio</Text>
                        <View style={styles.boxBio}>
                            <MCI name="github-circle" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.github}</Text>
                                <Text style={{ fontSize: 15, marginLeft: 9, color: '#929292' }}>{this.state.link_gh}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MCI name="gitlab" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.gitlab}</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292' }}>{this.state.link_gl}</Text>
                            </View>
                        </View>
                        <View style={styles.boxBio}>
                            <MCI name="web" style={{ fontSize: 18, marginLeft: 25 }} />
                            <View>
                                <Text style={{ fontSize: 15, marginLeft: 10, }}>{this.state.web}</Text>
                                <Text style={{ fontSize: 15, marginLeft: 10, color: '#929292', marginBottom: 10 }}>{this.state.link_web}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                {/* <StatusBar /> */}
            </View >
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },

    containerImage: {
        alignItems: 'center'
    },
    lineStyle: {
        borderWidth: 1,
        width: 330,
        borderColor: '#980080',
        elevation: 2,
        marginTop: 10
    },

    containerBio: {
        padding: 10
    },
    boxBio: {
        flexDirection: 'row',
        marginTop: 10
    }

})
