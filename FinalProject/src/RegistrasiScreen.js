import React from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, ScrollView, Button } from 'react-native';
import MCI from "@expo/vector-icons/MaterialCommunityIcons";

export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fullName: '',
            email: '',
            password: '',
            repeatPassword: '',
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Text style={{ color: '#980080', fontWeight: 'bold', fontSize: 25 }}>Registration</Text>
                </View>
                <View>
                    <View style={styles.inputContainer}>
                        <TextInput style={styles.textInput}
                            placeholder='Full name'
                            onChangeText={fullName => this.setState({ fullName })}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput style={styles.textInput}
                            placeholder='Email'
                            onChangeText={email => this.setState({ email })}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.textInput}
                            placeholder='Password'
                            onChangeText={password => this.setState({ password })}
                            secureTextEntry={true}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.textInput}
                            placeholder='Repeat password'
                            onChangeText={repeatPassword => this.setState({ repeatPassword })}
                            secureTextEntry={true}
                        />
                    </View>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.buttonLogin} onPress={() => this.props.navigation.navigate('Login')}>
                        <MCI color="white" name="account-circle" size={20} />
                        <Text style={styles.buttonText}> Sign In</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style={{ color: '#980080', fontSize: 15, marginTop: 10 }}>Already have account? Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F169CB',
        paddingBottom: 200,
    },
    logoContainer: {
        marginTop: 150,
        alignItems: 'center',
    },
    inputContainer: {
        marginTop: 20,
    },
    textInput: {
        width: 280,
        height: 38,
        borderColor: '#980080',
        borderWidth: 1,
        elevation: 6,
        borderRadius: 30,
        padding: 10,
        backgroundColor: 'white',
    },
    buttonContainer: {
        alignItems: 'center',
        paddingTop: 25,
    },
    buttonLogin: {
        height: 40,
        width: 150,
        marginTop: 10,
        backgroundColor: '#980080',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        elevation: 5,
    },
    buttonText: {
        color: 'white',
        fontSize: 18,
    }
});
